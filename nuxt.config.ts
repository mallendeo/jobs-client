import NuxtConfiguration from '@nuxt/config'

const isProd = process.env.NODE_ENV === 'production'

const config: NuxtConfiguration = {
  mode: 'universal',
  head: {
    title: '💻 Jobs - chile.sh',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'manifest',
        href: '/site.webmanifest'
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        type: 'image/png',
        sizes: '180x180',
        href: '/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png'
      }
    ]
  },
  loading: { color: '#fff' },
  css: ['~/assets/css/tailwind.css'],
  plugins: [],
  modules: ['@nuxtjs/axios', '@nuxtjs/eslint-module', '@nuxtjs/apollo'],

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: isProd
          ? 'https://jobs.chile.sh/graphql'
          : 'http://localhost:4000'
      }
    }
  },

  // https://axios.nuxtjs.org/options
  axios: {},

  build: {
    postcss: {
      plugins: {
        tailwindcss: './tailwind.config.js'
      }
    }
    // You can extend webpack config here
    // extend(config, ctx) {}
  }
}

export default config
